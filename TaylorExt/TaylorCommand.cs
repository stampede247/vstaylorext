﻿//------------------------------------------------------------------------------
// <copyright file="TaylorCommand.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.Design;
using System.Globalization;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace TaylorExt
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class TaylorCommand
    {
        public static readonly Guid CommandSetGuid = new Guid("1cabaeb7-6435-4796-8f64-8474cc40f8c6");
        private readonly Package package;

        //Singleton stuff
        public static TaylorCommand Instance { get; private set; }
        public static void Initialize(Package package) { Instance = new TaylorCommand(package); }

        /// <summary>
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        private TaylorCommand(Package package)
        {
            if (package == null) { throw new ArgumentNullException("package"); }
            this.package = package;

            OleMenuCommandService commandService = ((IServiceProvider)this.package).GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                CommandID taylorCmdId = new CommandID(CommandSetGuid, 0x100);
                MenuCommand taylorCmdMenuItem = new MenuCommand(this.TaylorCommandExecute, taylorCmdId);
                commandService.AddCommand(taylorCmdMenuItem);

                CommandID gotoEmptyLineId = new CommandID(CommandSetGuid, 0x101);
                MenuCommand gotoEmptyLineMenuItem = new MenuCommand(this.GotoEmptyLineExecute, gotoEmptyLineId);
                commandService.AddCommand(gotoEmptyLineMenuItem);
            }
        }

        private void TaylorCommandExecute(object sender, EventArgs e)
        {
            // Show a message box to prove we were here
            VsShellUtilities.ShowMessageBox(
                this.package,
                "This is the Taylor Command :P",
                "TaylorCommand",
                OLEMSGICON.OLEMSGICON_INFO,
                OLEMSGBUTTON.OLEMSGBUTTON_OK,
                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST
            );
        }

        private void GotoEmptyLineExecute(object sender, EventArgs e)
        {
            VsShellUtilities.ShowMessageBox(
                this.package,
                "This is the goto empty line command :O",
                "TaylorCommand",
                OLEMSGICON.OLEMSGICON_QUERY,
                OLEMSGBUTTON.OLEMSGBUTTON_YESNO,
                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST
            );
        }
    }
}
